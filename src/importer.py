from kafka import KafkaProducer, producer
from base64 import b64encode
from datetime import datetime
import json
import time
import requests
import logging
import os

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

def json_serializer(data):
    return json.dumps(data).encode("utf-8")

def prepare_and_send_msg(msg_json, producer):
    for j in range(len(msg_json['items'])):
        if(len(msg_json['items'][j]['_embedded']['categories']) != 0):
            post = {
                "categories": [
                    {
                        "id": msg_json['items'][j]['_embedded']['categories'][0]['id'],
                        "name": msg_json['items'][j]['_embedded']['categories'][0]['name'],
                        "parent": msg_json['items'][j]['_embedded']['categories'][0]['parent']
                    }
                ],
                "channel": msg_json['items'][j]['channel'],
                "closed_at": msg_json['items'][j]['closed_at'],
                "description": msg_json['items'][j]['description'],
                "expiry_at": msg_json['items'][j]['expiry_at'],
                "id": msg_json['items'][j]['id'],
                "modified_at": msg_json['items'][j]['modified_at'],
                "path": "/posts",
                "published_at": msg_json['items'][j]['published_at'],
                "source_type": "http",
                "status": msg_json['items'][j]['status'],
                "subject": msg_json['items'][j]['subject'],
                "timestamp": datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
                "type": msg_json['items'][j]['type'],
                "uuid": msg_json['items'][j]['uuid'],
                "version": 1
            }
            logging.info(f'segnalazioni_raw = ' + str(post) + "\n")
            producer.send(os.environ['KAFKA_TOPIC_SEGNALAZIONI_RAW'], post)

if __name__=="__main__":
    url_posts = "https://segnalazioni.comune.genova.it/api/sensor/posts?limit=10&cursor=%2A&embed=categories,owners"
    basic_auth = b64encode(bytes(os.environ['SEGNALACI_USERNAME'] + ":" + os.environ['SEGNALACI_PASSWORD'], "utf-8")).decode("ascii")
    producer = KafkaProducer(bootstrap_servers=[broker for broker in os.environ['KAFKA_BROKERS'].split(',')], value_serializer=json_serializer)
    counter=0
    
    response = requests.get(url_posts, headers={'Authorization': 'Basic ' + basic_auth}, timeout=20)
    json_posts_data = response.json()
    logging.info(json_posts_data)
    prepare_and_send_msg(json_posts_data, producer)   
    counter+=1
    while json_posts_data['next']:
        print("importer count: " + str(counter*10) + "\n")
        counter+=1
        response = requests.get(json_posts_data['next'] + "&embed=categories,owners", headers={'Authorization': 'Basic ' + basic_auth}, timeout=20)
        json_posts_data = response.json()
        prepare_and_send_msg(json_posts_data, producer)
